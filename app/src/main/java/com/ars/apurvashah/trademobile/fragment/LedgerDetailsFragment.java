package com.ars.apurvashah.trademobile.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ars.apurvashah.trademobile.Dashboard;
import com.ars.apurvashah.trademobile.R;
import com.ars.apurvashah.trademobile.adapters.LedgerDetailsAdapter;
import com.ars.apurvashah.trademobile.bean.LedgerDetails;
import com.ars.apurvashah.trademobile.networkConnection.ConnectionRequestParameters;
import com.ars.apurvashah.trademobile.networkConnection.ConnectionResponse;
import com.ars.apurvashah.trademobile.networkConnection.ConnectionUtils;
import com.ars.apurvashah.trademobile.utils.SharedPreferenceUtils;
import com.ars.apurvashah.trademobile.utils.Utils;

import java.util.ArrayList;


public class LedgerDetailsFragment extends Fragment {
    private static final String ARG_STRDPID = "strDPID";
    private static final String ARG_START_DATE = "startDate";
    private static final String ARG_END_DATE = "endDate";


    private String initDPID;
    private String initStartDate;
    private String initEndDate;
    private String userID;
    private Context context;
    private LedgerDetailsAdapter ledgerDetailsAdapter;

    public LedgerDetailsFragment() {
    }

    public static LedgerDetailsFragment newInstance(String strDPID, String startDate, String endDate) {
        LedgerDetailsFragment fragment = new LedgerDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_STRDPID, strDPID);
        args.putString(ARG_START_DATE, startDate);
        args.putString(ARG_END_DATE, endDate);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            initDPID = getArguments().getString(ARG_STRDPID);
            initStartDate = getArguments().getString(ARG_START_DATE);
            initEndDate = getArguments().getString(ARG_END_DATE);
        }
        context = getActivity();
        userID = SharedPreferenceUtils.getSharedPreference(context, getString(R.string.prefUserID));
    }

    RecyclerView ledgerDetailsRecyclerView;
    ProgressBar detailsProgressBar;
    LedgerDetailsFetchTask ledgerDetailsFetchTask;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.fragment_ledger_details, container, false);
        ledgerDetailsRecyclerView = (RecyclerView)fragmentView.findViewById(R.id.ledgerDetailsRecyclerView);
        ledgerDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        detailsProgressBar = (ProgressBar)fragmentView.findViewById(R.id.detailsProgressBar);
        ledgerDetailsFetchTask = new LedgerDetailsFetchTask(userID,initDPID,initStartDate,initEndDate);
        ledgerDetailsFetchTask.execute();
        return fragmentView;
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class LedgerDetailsFetchTask extends AsyncTask<Void, Void, ArrayList<LedgerDetails>> {

        private final String userID;
        private final String dpID;
        private final String startDate;
        private final String endDate;


        LedgerDetailsFetchTask(String userID, String dpID, String startDate, String endDate) {
            this.userID = userID;
            this.dpID = dpID;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(true);
        }

        @Override
        protected ArrayList<LedgerDetails> doInBackground(Void... params) {

            if(ConnectionUtils.checkInternetConnectivity(context)){
                ConnectionRequestParameters connectionRequestParameters = new ConnectionRequestParameters();
                String ledgerDetailsParameters = connectionRequestParameters.ledgerDetailsParameters(userID, dpID,startDate,endDate);
                ConnectionUtils connectionUtils = new ConnectionUtils();
                String responseString =  connectionUtils.connectAndGetResponse(context,2, ledgerDetailsParameters);
                ConnectionResponse connectionResponse = new ConnectionResponse();
                return  connectionResponse.getLedgerDetailsListFromResponse(responseString);
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<LedgerDetails> ledgerDetailsList) {
            onResultObtained(ledgerDetailsList);
        }
        @Override
        protected void onCancelled() {
            onResultObtained(null);
        }
    }

    private void onResultObtained(ArrayList<LedgerDetails> ledgerDetailsList) {
        if(isDetached() || isRemoving() || context == null){
            return;
        }
        ledgerDetailsFetchTask = null;
            if(ledgerDetailsList == null){
                Utils.showToast(context,"Unable to fetch information");
            }else{

                postOperationOnListAfterFetchingData(ledgerDetailsList);
                if(ledgerDetailsAdapter == null){
                    ledgerDetailsAdapter = new LedgerDetailsAdapter(context, ledgerDetailsList);
                }
                ledgerDetailsAdapter.setLedgerDetailsList(ledgerDetailsList);
                ledgerDetailsRecyclerView.setAdapter(ledgerDetailsAdapter);
                ledgerDetailsAdapter.notifyDataSetChanged();
                showProgress(false);
            }
    }

    private void postOperationOnListAfterFetchingData(ArrayList<LedgerDetails> ledgerDetailsList) {
        double totalBalance = 0.0;
        for(LedgerDetails ledgerDetails : ledgerDetailsList){
            if(!TextUtils.isEmpty(ledgerDetails.getLdAmount())) {
                ledgerDetails.setAmountInDouble(Double.parseDouble(ledgerDetails.getLdAmount()));

                if(!TextUtils.isEmpty(ledgerDetails.getLdDebitFlag()) && ledgerDetails.getLdDebitFlag().equals("D")){
                    ledgerDetails.setAmountInDouble(0 - ledgerDetails.getAmountInDouble());
                }
            }
            totalBalance += ledgerDetails.getAmountInDouble();
            ledgerDetails.setBalanceInDouble(totalBalance);
        }
    }

    private void showProgress(boolean showProgress) {
        if(showProgress){
            detailsProgressBar.setVisibility(View.VISIBLE);
            ledgerDetailsRecyclerView.setVisibility(View.INVISIBLE);
        }else{

            detailsProgressBar.setVisibility(View.INVISIBLE);
            ledgerDetailsRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getContext() instanceof Dashboard){
            ((Dashboard) getContext()).setHeader(getString(R.string.ledgerFor) + " "+initDPID);
        }
    }
}
