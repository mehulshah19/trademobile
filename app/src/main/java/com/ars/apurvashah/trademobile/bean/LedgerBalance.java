package com.ars.apurvashah.trademobile.bean;

/**
 * Created by Mehul on 21/06/16.
 */
public class LedgerBalance {

    String exchange;
    String bseCash;
    String nseCash;
    String nseDerivative;
    String nseFX;
    String total;
    String grandTotal;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getBseCash() {
        return bseCash;
    }

    public void setBseCash(String bseCash) {
        this.bseCash = bseCash;
    }

    public String getNseCash() {
        return nseCash;
    }

    public void setNseCash(String nseCash) {
        this.nseCash = nseCash;
    }

    public String getNseDerivative() {
        return nseDerivative;
    }

    public void setNseDerivative(String nseDerivative) {
        this.nseDerivative = nseDerivative;
    }

    public String getNseFX() {
        return nseFX;
    }

    public void setNseFX(String nseFX) {
        this.nseFX = nseFX;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }
}
