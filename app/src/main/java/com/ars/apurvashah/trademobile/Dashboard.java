package com.ars.apurvashah.trademobile;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ars.apurvashah.trademobile.fragment.DashboardFragment;
import com.ars.apurvashah.trademobile.fragment.LedgerBalanceFragment;
import com.ars.apurvashah.trademobile.fragment.LedgerDetailsFragment;
import com.ars.apurvashah.trademobile.utils.SharedPreferenceUtils;
import com.ars.apurvashah.trademobile.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Menu navigationMenu;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

         navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationMenu = navigationView.getMenu();
        callDashboardFragment();
        navHeaderLayout();
    }



    private void navHeaderLayout() {
        View headerView = navigationView.getHeaderView(0);
       TextView navUserName = (TextView) headerView.findViewById(R.id.navUserName);
        TextView navCompName = (TextView)headerView.findViewById(R.id.navCompName);
        TextView navUserID = (TextView) headerView.findViewById(R.id.navUserID);

        String storedUserName = SharedPreferenceUtils.getSharedPreference(this, getString(R.string.prefUserName));
        String storedCompName = SharedPreferenceUtils.getSharedPreference(this, getString(R.string.prefCompName));
        String storedUserId = SharedPreferenceUtils.getSharedPreference(this,getString(R.string.prefUserID));
        navUserName.setText(storedUserName);
        navCompName.setText(storedCompName);
        navUserID.setText(storedUserId);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        int id = item.getItemId();
        if(id == R.id.ledgerBalanceItem){
            clickedPositionOnGridOfDashoard(0);
        }else{
            clickedPositionOnGridOfDashoard(1);
        }
        return false;
    }

    public void callDashboardFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        DashboardFragment dashboardFragment = new DashboardFragment();
        fragmentTransaction.add(R.id.fragment_container, dashboardFragment);
     //   fragmentTransaction.addToBackStack("DashboardFragment");
        fragmentTransaction.commit();
    }


    public void callLedgerBalanceFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        LedgerBalanceFragment ledgerBalanceFragment = new LedgerBalanceFragment();
        fragmentTransaction.replace(R.id.fragment_container, ledgerBalanceFragment);
        fragmentTransaction.addToBackStack("LedgerBalanceFragment");
        fragmentTransaction.commit();
    }


    public void callLedgerDetailsFragment(String operation){


        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.DATE, 2); // Adding 2 days
        String next2DayDate = sdf.format(c.getTime());


        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
        Calendar c1 = Calendar.getInstance();
        c1.setTime(new Date()); // Now use today date.
        c1.add(Calendar.MONTH, -1); // Previous 1 month
        String previousMonthDate = sdf1.format(c1.getTime());

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        LedgerDetailsFragment ledgerDetailsFragment = LedgerDetailsFragment.newInstance(operation,previousMonthDate, next2DayDate);
        fragmentTransaction.replace(R.id.fragment_container, ledgerDetailsFragment);
        fragmentTransaction.addToBackStack("LedgerDetailsFragment");
        fragmentTransaction.commit();
    }


    public void setHeader(String header){
        if(TextUtils.isEmpty(header)){
            return;
        }
        getSupportActionBar().setTitle(header);
    }

    public void clickedPositionOnGridOfDashoard(int position) {
        if (position == 0) {
            callLedgerBalanceFragment();
        } else {
            Utils.showToast(Dashboard.this,"Work in Progress");
        }
    }
}
