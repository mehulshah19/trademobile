package com.ars.apurvashah.trademobile.fragment;

import android.content.Context;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ars.apurvashah.trademobile.Dashboard;
import com.ars.apurvashah.trademobile.R;
import com.ars.apurvashah.trademobile.adapters.LedgerDetailsAdapter;
import com.ars.apurvashah.trademobile.bean.LedgerBalance;
import com.ars.apurvashah.trademobile.networkConnection.ConnectionRequestParameters;
import com.ars.apurvashah.trademobile.networkConnection.ConnectionResponse;
import com.ars.apurvashah.trademobile.networkConnection.ConnectionUtils;
import com.ars.apurvashah.trademobile.utils.SharedPreferenceUtils;
import com.ars.apurvashah.trademobile.utils.Utils;

/**
 * Created by Mehul on 23/06/16.
 */
public class LedgerBalanceFragment extends Fragment implements View.OnClickListener {

    private Context context;
    private TableLayout balanceTableLayout;
    private TableRow exchangeTR;
    private TextView exchangeTV;
    private TableRow bseCashTR;
    private TextView bseCashTV;
    private TableRow nseCashTR;
    private TextView nseCashTV;
    private TableRow nseDerivativeTR;
    private TextView nseDerivativeTV;
    private TableRow nseFxTR;
    private TextView nseFXTV;
    private TableRow totalTR;
    private TextView totalTV;
    private TableRow grandTotalTR;
    private TextView grandTotalTV;

    // End Of Content View Elements

    private void bindViews(View view) {

        exchangeTR = (TableRow) view.findViewById(R.id.exchangeTR);
        exchangeTV = (TextView) view.findViewById(R.id.exchangeTV);
        bseCashTR = (TableRow) view.findViewById(R.id.bseCashTR);
        bseCashTV = (TextView) view.findViewById(R.id.bseCashTV);
        nseCashTR = (TableRow) view.findViewById(R.id.nseCashTR);
        nseCashTV = (TextView) view.findViewById(R.id.nseCashTV);
        nseDerivativeTR = (TableRow) view.findViewById(R.id.nseDerivativeTR);
        nseDerivativeTV = (TextView) view.findViewById(R.id.nseDerivativeTV);
        nseFxTR = (TableRow) view.findViewById(R.id.nseFxTR);
        nseFXTV = (TextView) view.findViewById(R.id.nseFXTV);
        totalTR = (TableRow) view.findViewById(R.id.totalTR);
        totalTV = (TextView) view.findViewById(R.id.totalTV);
        grandTotalTR = (TableRow) view.findViewById(R.id.grandTotalTR);
        grandTotalTV = (TextView) view.findViewById(R.id.grandTotalTV);
        balanceTableLayout = (TableLayout)view.findViewById(R.id.balanceTableLayout);
        bseCashTR.setOnClickListener(this);
        nseCashTR.setOnClickListener(this);
        nseDerivativeTR.setOnClickListener(this);
        nseFxTR.setOnClickListener(this);
    }



    public LedgerBalanceFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    RecyclerView ledgerDetailsRecyclerView;
    ProgressBar balanceProgressBar;
    LedgerBalanceAsyncTask ledgerBalanceAsyncTask;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.fragment_ledger_balance, container, false);
        bindViews(fragmentView);
        balanceProgressBar = (ProgressBar)fragmentView.findViewById(R.id.balanceProgressBar);
        String userID = SharedPreferenceUtils.getSharedPreference(context,getString(R.string.prefUserID));
        ledgerBalanceAsyncTask = new LedgerBalanceAsyncTask(userID);
        ledgerBalanceAsyncTask.execute();
        return fragmentView;
    }

    @Override
    public void onClick(View v) {
        String clickedString = "";
        switch (v.getId()){
            case R.id.bseCashTR:
                clickedString = "BSE-Cash";
                break;
            case R.id.nseCashTR:
                clickedString = "NSE-Cash";
                break;
            case R.id.nseDerivativeTR:
                clickedString = "NSE-DERIVATIVE";
                break;
            case R.id.nseFxTR:
                clickedString = "NSE-FX";
                break;

        }
        if(getContext() instanceof Dashboard){
            ((Dashboard)getContext()).callLedgerDetailsFragment(clickedString);
        }
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class LedgerBalanceAsyncTask extends AsyncTask<Void, Void,LedgerBalance> {

        private final String userID;


        LedgerBalanceAsyncTask(String userID) {
            this.userID = userID;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(true);
        }

        @Override
        protected LedgerBalance doInBackground(Void... params) {

            if(ConnectionUtils.checkInternetConnectivity(context)){
                ConnectionRequestParameters connectionRequestParameters = new ConnectionRequestParameters();
                String ledgerDetailsParameters = connectionRequestParameters.ledgerBalanceParameters(userID);
                ConnectionUtils connectionUtils = new ConnectionUtils();
                String responseString =  connectionUtils.connectAndGetResponse(context,1, ledgerDetailsParameters);
                ConnectionResponse connectionResponse = new ConnectionResponse();
                return  connectionResponse.getLedgerBalanceObjectFromResponse(responseString);
            }
            return null;
        }

        @Override
        protected void onPostExecute(LedgerBalance ledgerBalance) {
            onResultObtained(ledgerBalance);
        }
        @Override
        protected void onCancelled() {
            onResultObtained(null);
        }
    }

    private void onResultObtained(LedgerBalance ledgerBalance) {
        if(isDetached() || isRemoving() || context == null){
            return;
        }
        ledgerBalanceAsyncTask = null;
        if(ledgerBalance == null){
            Utils.showToast(context,"Unable to fetch information");
        }else{

            exchangeTV.setText(ledgerBalance.getExchange());
//            bseCashTV.setText(checkIfZeroToAddSpace(ledgerBalance.getBseCash()));
//            nseCashTV.setText(checkIfZeroToAddSpace(ledgerBalance.getNseCash()));
//            nseDerivativeTV.setText(checkIfZeroToAddSpace(ledgerBalance.getNseDerivative()));
//            nseFXTV.setText(checkIfZeroToAddSpace(ledgerBalance.getNseFX()));
            checkIfZeroToAddSpace(bseCashTV, ledgerBalance.getBseCash());
            checkIfZeroToAddSpace(nseCashTV,ledgerBalance.getNseCash());
            checkIfZeroToAddSpace(nseDerivativeTV, ledgerBalance.getNseDerivative());
            checkIfZeroToAddSpace(nseFXTV,ledgerBalance.getNseFX());

            totalTV.setText(ledgerBalance.getTotal());
            grandTotalTV.setText(ledgerBalance.getGrandTotal());


            showProgress(false);
        }
    }

    public void checkIfZeroToAddSpace(TextView textView, String value){
        String textValue = "";
        if(TextUtils.isEmpty(value) || value.trim().equals("0")) {
                textValue = "0     ";
            }else {
            textView.setPaintFlags(nseFXTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            textValue = value;
        }
        textView.setText(textValue);
    }

    private void showProgress(boolean showProgress) {
        if(showProgress){
            balanceProgressBar.setVisibility(View.VISIBLE);
            balanceTableLayout.setVisibility(View.INVISIBLE);
        }else{

            balanceProgressBar.setVisibility(View.INVISIBLE);
            balanceTableLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getContext() instanceof  Dashboard){
            ((Dashboard) getContext()).setHeader(getString(R.string.ledgerBalance));
        }
    }
}
