package com.ars.apurvashah.trademobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ars.apurvashah.trademobile.networkConnection.ConnectionRequestParameters;
import com.ars.apurvashah.trademobile.networkConnection.ConnectionResponse;
import com.ars.apurvashah.trademobile.networkConnection.ConnectionUtils;
import com.ars.apurvashah.trademobile.utils.SharedPreferenceUtils;
import com.ars.apurvashah.trademobile.utils.Utils;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private UserLoginTask mAuthTask = null;

    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        showProgress(false);
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String userID = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) || TextUtils.isEmpty(password.trim())) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }
        if (TextUtils.isEmpty(userID) || TextUtils.isEmpty(userID.trim())) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            mAuthTask = new UserLoginTask(userID.trim(), password.trim());
            mAuthTask.execute((Void) null);
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        if(show){
            mProgressView.setVisibility(View.VISIBLE);
        }else{
            mProgressView.setVisibility(View.INVISIBLE);
        }
    }
    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            if(ConnectionUtils.checkInternetConnectivity(LoginActivity.this)){
                ConnectionRequestParameters connectionRequestParameters = new ConnectionRequestParameters();
                String validCredentialsParameters = connectionRequestParameters.validUsersParameters(mEmail, mPassword);
                ConnectionUtils connectionUtils = new ConnectionUtils();
                String responseString =  connectionUtils.connectAndGetResponse(LoginActivity.this,0,validCredentialsParameters);
                ConnectionResponse connectionResponse = new ConnectionResponse();
                return  connectionResponse.isUserValid(LoginActivity.this, responseString);
           }

            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            if (success) {
                if(LoginActivity.this != null) {
                    Intent dashboardIntent = new Intent(LoginActivity.this, Dashboard.class);
                    startActivity(dashboardIntent);
                    SharedPreferenceUtils.putSharedPreferenceForString(LoginActivity.this, getString(R.string.prefUserID),mEmailView.getText().toString().trim().toUpperCase());
                    finish();
                }
            } else {
                Utils.showToast(LoginActivity.this,getString(R.string.invalidCredentials));
            }
        }
        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

