package com.ars.apurvashah.trademobile.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.ars.apurvashah.trademobile.Dashboard;
import com.ars.apurvashah.trademobile.R;
import com.ars.apurvashah.trademobile.adapters.DashboardAdapter;

/**
 * Created by Mehul on 25/06/16.
 */
public class DashboardFragment extends Fragment {

    GridView dashboardListGridView;
    DashboardAdapter dashboardAdapter;
    private String[] dashboardList;

    public DashboardFragment(){

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_dashboard, container, false);
        dashboardListGridView = (GridView) rootView.findViewById(R.id.gridView);
        dashboardList = getContext().getResources().getStringArray(R.array.dashboardList);
        dashboardAdapter = new DashboardAdapter(getActivity(), dashboardList);
        dashboardListGridView.setAdapter(dashboardAdapter);
        dashboardListGridView.setNumColumns(3);

        dashboardListGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onItemClickForDashboardGridView(position);

            }
        });
        return rootView;
    }

    private void onItemClickForDashboardGridView(int position) {
            if(getContext() instanceof Dashboard) {
                ((Dashboard)getContext()).clickedPositionOnGridOfDashoard(position);
            }
        }

    @Override
    public void onResume() {
        super.onResume();
        if(getContext() instanceof  Dashboard){
            ((Dashboard) getContext()).setHeader(getString(R.string.title_activity_dashboard));
        }
    }
}
