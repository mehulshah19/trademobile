package com.ars.apurvashah.trademobile.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.ArrayList;

public class SharedPreferenceUtils {

    private static final String MyPREFERENCES = "TRadeMobile_Preferences";
    private static final String prefixString = "####";

    public static void putSharedPreferenceForString(Context context, String key, String addingParameter){
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, addingParameter.toString());
        editor.commit();
    }

    public static String getSharedPreference(Context context,String key){
        if(context == null){
            return "";
        }
        SharedPreferences  sharedPreferences = context.getApplicationContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }


    public static void saveStringInStringArray(Context context, ArrayList<String> arrayList , String key)
    {
        if(arrayList == null || arrayList.size() == 0){
            return;
        }
        String arrayString = "";
        for(String singleItem : arrayList) {
            arrayString += prefixString + singleItem;
        }
        putSharedPreferenceForString(context, key, arrayString);
    }


    public static ArrayList<String> loadListFromKey(Context context, String key) {
        ArrayList<String> stringList = new ArrayList<String>();
        if (TextUtils.isEmpty(key))
            return stringList;

        String fetchedString = getSharedPreference(context, key);
        if (TextUtils.isEmpty(fetchedString))
            return stringList;

        String fetchedStringWithRemovalOfPrefix = fetchedString.replaceFirst(prefixString, "");
        for (String stringElement : fetchedStringWithRemovalOfPrefix.split(prefixString)) {
            stringList.add(stringElement);
        }
        return stringList;
    }
}

