package com.ars.apurvashah.trademobile.networkConnection;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.ars.apurvashah.trademobile.R;
import com.ars.apurvashah.trademobile.bean.LedgerBalance;
import com.ars.apurvashah.trademobile.bean.LedgerDetails;
import com.ars.apurvashah.trademobile.utils.SharedPreferenceUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Mehul on 21/06/16.
 */
public class ConnectionResponse {


    private String convertResponseXMLToString(String responseXml){
        if(TextUtils.isEmpty(responseXml)){
            return null;
        }
        try{
            NodeList nl = getDomElement(responseXml).getElementsByTagName("string");
            Element e = (Element) nl.item(0);
            return e.getTextContent();
        }catch(Exception e){
            Log.e("Exception", e.getMessage());
        }
        return null;
    }



    public Document getDomElement(String xml){
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        }
        // return DOM
        return doc;
    }

    public boolean isUserValid(Context context, String responseXML){
        if(context == null){
            return false;
        }
        if(TextUtils.isEmpty(responseXML)){
            return false;
        }
        String removedUnwantedResponseString = convertResponseXMLToString(responseXML);

        if(TextUtils.isEmpty(removedUnwantedResponseString) || removedUnwantedResponseString.toLowerCase().contains("InValid Password/PAN NO.".toLowerCase())){
            return false;
        }else{
            String[] arrayOfRowsEntry = removedUnwantedResponseString.split("\\|");
            if(arrayOfRowsEntry == null || arrayOfRowsEntry.length == 0){
                return true;
            }
            String dataRow = arrayOfRowsEntry[1];

            String [] arrayOfColumnValues = dataRow.split("\\~");
            if(arrayOfColumnValues == null){
                return true;
            }

            SharedPreferenceUtils.putSharedPreferenceForString(context,context.getString(R.string.prefCompName), arrayOfColumnValues[0].trim());
            SharedPreferenceUtils.putSharedPreferenceForString(context,context.getString(R.string.prefUserID), arrayOfColumnValues[1].trim());
            SharedPreferenceUtils.putSharedPreferenceForString(context,context.getString(R.string.prefUserName), arrayOfColumnValues[2].trim());

            return true;
        }
    }

    public LedgerBalance getLedgerBalanceObjectFromResponse(String responseXML){
        if(TextUtils.isEmpty(responseXML)){
            return null;
        }
        String removedUnwantedResponseString = convertResponseXMLToString(responseXML);
        if(!TextUtils.isEmpty(removedUnwantedResponseString)) {
            LedgerBalance ledgerBalance = null;
            String[] arrayOfRowsEntry = removedUnwantedResponseString.split("\\|");
            ArrayList<String> titleList = new ArrayList<>();
            ArrayList<String> descriptionList = new ArrayList<>();

            for(String singleRow : arrayOfRowsEntry){
                String[] columnsValue = singleRow.split("\\~");
                if(columnsValue.length >0){
                    titleList.add(columnsValue[0].trim());
                    descriptionList.add(columnsValue[1].trim());
                }
            }

            if(titleList.size() > 0 && descriptionList.size() >0 && titleList.size() == 7 &&  descriptionList.size() == 7){
                ledgerBalance = new LedgerBalance();
                ledgerBalance.setExchange(descriptionList.get(0));
                ledgerBalance.setBseCash(descriptionList.get(1));
                ledgerBalance.setNseCash(descriptionList.get(2));
                ledgerBalance.setNseDerivative(descriptionList.get(3));
                ledgerBalance.setNseFX(descriptionList.get(4));
                ledgerBalance.setTotal(descriptionList.get(5));
                ledgerBalance.setGrandTotal(descriptionList.get(6));
            }

            return ledgerBalance;

        }else{
            return null;
        }
    }


    public ArrayList<LedgerDetails> getLedgerDetailsListFromResponse(String responseXML){
        ArrayList<LedgerDetails> ledgerDetailsList = new ArrayList<>();
        if(TextUtils.isEmpty(responseXML)){
            return ledgerDetailsList;
        }
         String removedUnwantedResponseString = convertResponseXMLToString(responseXML);
        if(!TextUtils.isEmpty(removedUnwantedResponseString)) {
            String[] arrayOfRowsEntry = removedUnwantedResponseString.split("\\|");
            for(int i=1; i<arrayOfRowsEntry.length; i++){
                String singleRow = arrayOfRowsEntry[i];
                LedgerDetails ledgerDetails =  getLedgeDetailsbeanFromRowData(singleRow);
                if(ledgerDetails != null){
                    ledgerDetailsList.add(ledgerDetails);
                }
            }
        }
        return ledgerDetailsList;
    }

    private LedgerDetails getLedgeDetailsbeanFromRowData(String singleRow) {
        if(TextUtils.isEmpty(singleRow)){
            return null;
        }

        String [] arrayOfColumnValues = singleRow.split("\\~");
        if(arrayOfColumnValues.length > 14){
            return null;
        }

        for(int i=0; i<arrayOfColumnValues.length; i++){
            arrayOfColumnValues[i] = arrayOfColumnValues[i].trim();
        }

        LedgerDetails ledgerDetails = new LedgerDetails();
        ledgerDetails.fromDate = arrayOfColumnValues[0];
        ledgerDetails.toDate = arrayOfColumnValues[1];
        ledgerDetails.exCode = arrayOfColumnValues[2];
        ledgerDetails.exchangeTitle = arrayOfColumnValues[3];
        ledgerDetails.account = arrayOfColumnValues[4];
        ledgerDetails.ldClientCD = arrayOfColumnValues[5];
        ledgerDetails.ldDt = arrayOfColumnValues[6];
        ledgerDetails.ldAmount = arrayOfColumnValues[7];
        ledgerDetails.ldParticular = arrayOfColumnValues[8];
        ledgerDetails.ldDebitFlag = arrayOfColumnValues[9];
        ledgerDetails.ldChequeNo = arrayOfColumnValues[10];
        ledgerDetails.ldDocumentType = arrayOfColumnValues[11];
        ledgerDetails.ldCommon = arrayOfColumnValues[12];
        ledgerDetails.lDate = arrayOfColumnValues[13];
        return ledgerDetails;
    }
}
