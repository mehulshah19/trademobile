package com.ars.apurvashah.trademobile.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ars.apurvashah.trademobile.R;


/**
 * Created by mehulshah on 24/07/15.
 */
public class DashboardAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    Context context;
    String[] dashboardTitleList;

    public DashboardAdapter(Context context, String[] dashboardTitleList){
        this.context = context;
        this.dashboardTitleList = dashboardTitleList;
        inflater = (LayoutInflater)context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return dashboardTitleList.length;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return dashboardTitleList[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view =null;
        ViewHolder viewHolder;
        if(view == null){
            view =  inflater.inflate(R.layout.adapter_dashboard_single_card, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView)view.findViewById(R.id.title);
            viewHolder.icon = (ImageView)view.findViewById(R.id.icon);
            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if(!TextUtils.isEmpty(dashboardTitleList[position])) {
            viewHolder.title.setText(dashboardTitleList[position]);
        }else{
            viewHolder.title.setText(" - ");
        }

        switch(position){
            case 0:
                viewHolder.icon.setImageResource(R.drawable.income_icon);
                break;
            case 1:
                viewHolder.icon.setImageResource(R.drawable.expense_icon);
                break;
            case 2:
                viewHolder.icon.setImageResource(R.drawable.overview_logo);
                break;
            case 3:
                viewHolder.icon.setImageResource(R.drawable.claim_management_icon);
                break;
            case 4:
                viewHolder.icon.setImageResource(R.drawable.ic_reminder);
                break;
            case 5:
                viewHolder.icon.setImageResource(R.drawable.card_details_icon);
                break;
        }



        return view;
    }

    static class ViewHolder{
        TextView title;
        ImageView icon;
    }
}

