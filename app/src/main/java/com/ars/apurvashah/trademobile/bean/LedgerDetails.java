package com.ars.apurvashah.trademobile.bean;

/**
 * Created by Mehul on 21/06/16.
 */
public class LedgerDetails {

    public String fromDate;
    public String toDate;
    public String exCode;
    public String exchangeTitle;
    public String account;
    public String ldClientCD;
    public String ldDt;
    public String ldAmount;
    public String ldParticular;
    public String ldDebitFlag;
    public String ldChequeNo;
    public String ldDocumentType;
    public String ldCommon;
    public String lDate;

    public double balanceInDouble;
    public double amountInDouble;

    public double getBalanceInDouble() {
        return balanceInDouble;
    }

    public void setBalanceInDouble(double balanceInDouble) {
        this.balanceInDouble = balanceInDouble;
    }

    public double getAmountInDouble() {
        return amountInDouble;
    }

    public void setAmountInDouble(double amountInDouble) {
        this.amountInDouble = amountInDouble;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getExCode() {
        return exCode;
    }

    public void setExCode(String exCode) {
        this.exCode = exCode;
    }

    public String getExchangeTitle() {
        return exchangeTitle;
    }

    public void setExchangeTitle(String exchangeTitle) {
        this.exchangeTitle = exchangeTitle;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getLdClientCD() {
        return ldClientCD;
    }

    public void setLdClientCD(String ldClientCD) {
        this.ldClientCD = ldClientCD;
    }

    public String getLdDt() {
        return ldDt;
    }

    public void setLdDt(String ldDt) {
        this.ldDt = ldDt;
    }

    public String getLdAmount() {
        return ldAmount;
    }

    public void setLdAmount(String ldAmount) {
        this.ldAmount = ldAmount;
    }

    public String getLdParticular() {
        return ldParticular;
    }

    public void setLdParticular(String ldParticular) {
        this.ldParticular = ldParticular;
    }

    public String getLdDebitFlag() {
        return ldDebitFlag;
    }

    public void setLdDebitFlag(String ldDebitFlag) {
        this.ldDebitFlag = ldDebitFlag;
    }

    public String getLdChequeNo() {
        return ldChequeNo;
    }

    public void setLdChequeNo(String ldChequeNo) {
        this.ldChequeNo = ldChequeNo;
    }

    public String getLdDocumentType() {
        return ldDocumentType;
    }

    public void setLdDocumentType(String ldDocumentType) {
        this.ldDocumentType = ldDocumentType;
    }

    public String getLdCommon() {
        return ldCommon;
    }

    public void setLdCommon(String ldCommon) {
        this.ldCommon = ldCommon;
    }

    public String getlDate() {
        return lDate;
    }

    public void setlDate(String lDate) {
        this.lDate = lDate;
    }
}
