package com.ars.apurvashah.trademobile.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Mehul on 23/06/16.
 */
public class Utils {

    public static void showToast(Context context,String text){
        if(context == null){
            return;
        }
        Toast.makeText(context,text,Toast.LENGTH_SHORT).show();
    }


    public static String convertDoubleToStringAppendingCrDr(Double value){
        Double twoDigitDecimalValue = roundToTwoDecimalPlace(value,2);
        if(twoDigitDecimalValue < 0){
            twoDigitDecimalValue = 0 - twoDigitDecimalValue;
            String currencyFormattedString = getCurrencyFormattedString(twoDigitDecimalValue);
        //    return twoDigitDecimalValue + " Dr";
            return currencyFormattedString +" Dr";
        }
        if(twoDigitDecimalValue > 0){
   //         return twoDigitDecimalValue + " Cr";
            String currencyFormattedString = getCurrencyFormattedString(twoDigitDecimalValue);
            //    return twoDigitDecimalValue + " Dr";
            return currencyFormattedString +" Cr";
        }
        else{
            return twoDigitDecimalValue + "   ";
        }
    }


    public static String getCurrencyFormattedString(double value){
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
        String currency = numberFormat.format(value).toLowerCase();
        String stringPlace = currency.substring(4,currency.length());
        return stringPlace;
    }

    public static double roundToTwoDecimalPlace(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static String changeThePositionOfDate(String originalDate){
        Log.e("tjj", "");
        String[] dateElements = originalDate.split("/");
        if(dateElements.length == 3){
            return dateElements[1]+"/"+dateElements[0]+"/"+dateElements[2];
         }else {
            return originalDate;
        }
    }
}
