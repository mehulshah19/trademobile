package com.ars.apurvashah.trademobile.networkConnection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.ars.apurvashah.trademobile.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by Mehul on 19/06/16.
 */
public class ConnectionUtils {
    private static final char PARAMETER_DELIMITER = '&';
    private static final char PARAMETER_EQUALS_CHAR = '=';


    public static boolean checkInternetConnectivity(Context context) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }else{
                return false;
            }
        }



        public String connectAndGetResponse(Context context, int operation, String postParameters) {
            HttpURLConnection conn = null;
            String urlString = null;
            try {
                if(operation == 0){
                     urlString  =  context.getString(R.string.urlHost) + "GetUserProfile";
                }else if(operation == 1){
                    urlString  = context.getString(R.string.urlHost) + "GetLedgerBalance";
                }
                else if(operation == 2){
                    urlString = context.getString(R.string.urlHost) + "GetLedgerDetailsO";
                }
                URL url = new URL(urlString);
                conn =  (HttpURLConnection) url.openConnection();

                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                if (postParameters != null) {
                    conn.setFixedLengthStreamingMode(
                            postParameters.getBytes().length);
                    conn.setRequestProperty("Content-Type",
                            "application/x-www-form-urlencoded");

                    //send the POST out
                    PrintWriter out = new PrintWriter(conn.getOutputStream());
                    out.print(postParameters);
                    out.close();
                }
                int statusCode = conn.getResponseCode();
                if (statusCode != HttpURLConnection.HTTP_OK) {
                    return null;
                }

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return response.toString();
            } catch (MalformedURLException m) {
                Log.e("MalformedURLException", m.getMessage());

            } catch (SocketException s) {
                Log.e("SocketException", s.getMessage());
            } catch (IOException i) {
                Log.e("IO Exception", i.getMessage());
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
        return null;
        }

    public static String createQueryStringForParameters(Map<String, String> parameters) {
        StringBuilder parametersAsQueryString = new StringBuilder();
        if (parameters != null) {
            boolean firstParameter = true;

            for (String parameterName : parameters.keySet()) {
                if (!firstParameter) {
                    parametersAsQueryString.append(PARAMETER_DELIMITER);
                }

                parametersAsQueryString.append(parameterName)
                        .append(PARAMETER_EQUALS_CHAR)
                        .append(URLEncoder.encode(
                                parameters.get(parameterName)));

                firstParameter = false;
            }
        }
        return parametersAsQueryString.toString();
    }
}



