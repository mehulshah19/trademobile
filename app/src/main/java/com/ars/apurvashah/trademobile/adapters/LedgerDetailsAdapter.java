package com.ars.apurvashah.trademobile.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ars.apurvashah.trademobile.R;
import com.ars.apurvashah.trademobile.bean.LedgerDetails;
import com.ars.apurvashah.trademobile.utils.Utils;

import java.util.List;

/**
 * Created by Mehul on 23/06/16.
 */public class LedgerDetailsAdapter extends RecyclerView.Adapter<LedgerDetailsAdapter.LedgerDetailsViewHolder> {
    public void setLedgerDetailsList(List<LedgerDetails> ledgerDetailsList) {
        this.ledgerDetailsList = ledgerDetailsList;
    }

    private List<LedgerDetails> ledgerDetailsList = null;
    private final LayoutInflater inflater;
    private Context context;
    TypedArray drawableList;
    int key;


    //Constructor to get need params
    public LedgerDetailsAdapter(Context context, List<LedgerDetails> ledgerDetailsList){
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.ledgerDetailsList = ledgerDetailsList;
    }
    LedgerDetailsViewHolder holder;


    //create a holder to organize views
    @Override
    public LedgerDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.adapter_ledger_details_row, parent, false);
        holder = new LedgerDetailsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LedgerDetailsViewHolder holder, int position) {
        LedgerDetails ledgerDetails = ledgerDetailsList.get(position);

        if(!TextUtils.isEmpty(ledgerDetails.getlDate())){
            holder.dateTV.setText(Utils.changeThePositionOfDate(ledgerDetails.getlDate()));
        }else{
            holder.dateTV.setText("-");
        }

        if(!TextUtils.isEmpty(ledgerDetails.getLdParticular())){
            holder.descriptionTV.setText(ledgerDetails.getLdParticular());
        }else{
            holder.descriptionTV.setText("-");
        }

        if(!TextUtils.isEmpty(ledgerDetails.getLdChequeNo())){
            holder.chequeNoTV.setText(ledgerDetails.getLdChequeNo());
        }else{
            holder.chequeNoTV.setText("-");
        }

        holder.amountTV.setText(Utils.convertDoubleToStringAppendingCrDr(ledgerDetails.getAmountInDouble()));
        holder.balanceTV.setText(Utils.convertDoubleToStringAppendingCrDr(ledgerDetails.getBalanceInDouble()));
    }

    @Override
    public int getItemCount() {
        return ledgerDetailsList.size();
    }


    //is a class act as an ViewHolder for Recycler View's Adapter.
    class LedgerDetailsViewHolder extends RecyclerView.ViewHolder {
         TextView dateTV;
         TextView amountTV;
         TextView descriptionTV;
         TextView chequeNoTV;
         TextView balanceTV;

        // End Of Content View Elements

        private void bindViews(View itemView) {

            dateTV = (TextView) itemView.findViewById(R.id.dateTV);
            amountTV = (TextView) itemView.findViewById(R.id.amountTV);
            descriptionTV = (TextView) itemView.findViewById(R.id.descriptionTV);
            chequeNoTV = (TextView) itemView.findViewById(R.id.chequeNoTV);
            balanceTV = (TextView) itemView.findViewById(R.id.balanceTV);
        }

        public LedgerDetailsViewHolder(View itemView) {
            super(itemView);
            // if (key == 0)
            bindViews(itemView);
        }
    }
}
