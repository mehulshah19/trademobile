package com.ars.apurvashah.trademobile.networkConnection;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mehul on 20/06/16.
 */
public class ConnectionRequestParameters {

    public String validUsersParameters(String userID, String password){
        if(TextUtils.isEmpty(userID) || TextUtils.isEmpty(password)){
            return null;
        }
        Map<String, String> validUserCredentialsMap = new HashMap<>();
        validUserCredentialsMap.put("struserid", userID.trim().toUpperCase());
        validUserCredentialsMap.put("strpwd", password.trim().toUpperCase());
        return ConnectionUtils.createQueryStringForParameters(validUserCredentialsMap);
    }


    public String ledgerBalanceParameters(String userID){
        if(TextUtils.isEmpty(userID)){
            return null;
        }
        Map<String, String> validUserCredentialsMap = new HashMap<>();
        validUserCredentialsMap.put("struserid", userID.trim().toUpperCase());
        return ConnectionUtils.createQueryStringForParameters(validUserCredentialsMap);
    }

    public String ledgerDetailsParameters(String userID, String strDPID, String startDate, String endDate){
        if(TextUtils.isEmpty(userID) || TextUtils.isEmpty(strDPID) || TextUtils.isEmpty(startDate) || TextUtils.isEmpty(endDate)){
            return null;
        }
        Map<String, String> validUserCredentialsMap = new HashMap<>();
        validUserCredentialsMap.put("struserid", userID.trim().toUpperCase());
        validUserCredentialsMap.put("strDPID", strDPID.trim().toUpperCase());
        validUserCredentialsMap.put("strFromDt", startDate.trim().toUpperCase());
        validUserCredentialsMap.put("strToDt", endDate.trim().toUpperCase());
        return ConnectionUtils.createQueryStringForParameters(validUserCredentialsMap);
    }
}
